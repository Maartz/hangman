package main

import (
	"fmt"
	"hangman/dictionnary"
	"hangman/hangman"
	"os"
)

func main() {

	err := dictionnary.Load("words.txt")
	if err != nil {
		fmt.Printf("Could not load dictionnary: %v\n", err)
		os.Exit(1)
	}

	g, err := hangman.New(8, dictionnary.PickRandWord())
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	hangman.DrawWelcome()
	prop := ""
	for {
		hangman.Draw(g, prop)

		switch g.State {
		case "won", "lost":
			os.Exit(0)
		}
		l, err := hangman.ReadGuess()
		if err != nil {
			fmt.Printf("Could not read from terminal: %v", err)
			os.Exit(1)
		}
		prop = l
		g.MakeAProp(prop)
	}
}
