package hangman

import (
	"fmt"
	"strings"
)

type Game struct {
	State        string
	Letters      []string
	FoundLetters []string
	UsedLetters  []string
	TurnsLeft    int
}

func New(turn int, word string) (*Game, error) {
	if len(word) < 3 {
		return nil, fmt.Errorf("word %s must be at least 3 characters long. Got: %v", word, len(word))
	}
	ltrs := strings.Split(strings.ToUpper(word), "")
	fnd := make([]string, len(ltrs))
	for i := 0; i < len(ltrs); i++ {
		fnd[i] = "_"
	}
	g := &Game{
		State:        "",
		Letters:      ltrs,
		FoundLetters: fnd,
		UsedLetters:  []string{},
		TurnsLeft:    turn,
	}
	return g, nil
}

func (g *Game) MakeAProp(prop string) {
	prop = strings.ToUpper(prop)

	switch g.State {
	case "won", "lost":
		return
	}

	if letterInWord(prop, g.UsedLetters) {
		g.State = "alreadyProposed"
	} else if letterInWord(prop, g.Letters) {
		g.State = "goodProp"
		g.RevealLetter(prop)
		if hasWon(g.Letters, g.FoundLetters) {
			g.State = "won"
		}
	} else {
		g.State = "badProp"
		g.TurnsLeft--
		g.RevealLetter(prop)
		if g.TurnsLeft <= 0 {
			g.State = "lost"
		}
	}
}

func letterInWord(p string, ltrs []string) bool {
	for _, l := range ltrs {
		if l == p {
			return true
		}
	}
	return false
}

func (g *Game) RevealLetter(p string) {
	g.UsedLetters = append(g.UsedLetters, p)
	for i, l := range g.Letters {
		if l == p {
			g.FoundLetters[i] = p
		}
	}
}

func hasWon(ltrs []string, fdLt []string) bool {
	for i := range ltrs {
		if ltrs[i] != fdLt[i] {
			return false
		}
	}
	return true
}
