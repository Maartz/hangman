package hangman

import "testing"

func TestLetterInWord(t *testing.T)  {
	w := []string{"b", "a", "d"}
	prop := "d"
	hasLetter := letterInWord(prop, w)
	if !hasLetter {
		t.Errorf("Word %s contains letter %s. got: %v", w,prop, hasLetter)
	}
}

func TestLetterNotInWord(t *testing.T)  {
	w := []string{"b", "a", "d"}
	prop := "t"
	hasLetter := letterInWord(prop, w)
	if hasLetter {
		t.Errorf("Word %s does not contains letter %s. got: %v", w,prop, hasLetter)
	}
}

func TestInvalidWord(t *testing.T){
	_, err := New(3, "")
	if err == nil{
		t.Errorf("Error should be returned when using an invalid word")
	}
}

func TestGameGoodProp(t *testing.T){
	g, _ := New(3, "bob")
	g.MakeAProp("b")
	validState(t, "goodProp", g.State)
}

func TestGameAlreadyProp(t *testing.T){
	g, _ := New(3, "abcde")
	g.MakeAProp("a")
	g.MakeAProp("a")
	validState(t, "alreadyProposed", g.State)
}

func TestGameBadProp(t *testing.T){
	g, _ := New(3, "abcde")
	g.MakeAProp("j")
	validState(t, "badProp", g.State)
}

func TestGameWon(t *testing.T){
	g, _ := New(3, "bob")
	g.MakeAProp("b")
	g.MakeAProp("o")
	validState(t, "won", g.State)
}

func TestGameLost(t *testing.T){
	g, _ := New(3, "bob")
	g.MakeAProp("x")
	g.MakeAProp("a")
	g.MakeAProp("c")
	validState(t, "lost", g.State)
}

func validState(t *testing.T, expS, actS string) bool {
	if expS != actS {
		t.Errorf("State should be %v. Got %v", expS, actS)
		return false
	}
	return true
}