package hangman

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

var r = bufio.NewReader(os.Stdin)

func ReadGuess() (prop string, err error){
	valid := false
	for !valid {
		fmt.Print("What is your proposition ? ")
		prop, err = r.ReadString('\n')
		if err != nil {
			return prop, err
		}
		prop = strings.TrimSpace(prop)
		if len(prop) != 1 {
			fmt.Printf("Invalid input. letter: %v, len: %v\n", prop, len(prop))
			continue
		}
		valid = true
	}
	return prop, nil
}