package hangman

import "fmt"

func DrawWelcome() {
	fmt.Print(`
          _______  _        _______  _______  _______  _       
|\     /|(  ___  )( (    /|(  ____ \(       )(  ___  )( (    /|
| )   ( || (   ) ||  \  ( || (    \/| () () || (   ) ||  \  ( |
| (___) || (___) ||   \ | || |      | || || || (___) ||   \ | |
|  ___  ||  ___  || (\ \) || | ____ | |(_)| ||  ___  || (\ \) |
| (   ) || (   ) || | \   || | \_  )| |   | || (   ) || | \   |
| )   ( || )   ( || )  \  || (___) || )   ( || )   ( || )  \  |
|/     \||/     \||/    )_)(_______)|/     \||/     \||/    )_)
                                                               
`)
}

func Draw(g *Game, prop string){
	drawTurns(g.TurnsLeft)
	drawState(g, prop)
}

func drawTurns(l int) {
	var drw string
	switch l {
	case 0:
		drw = `
    ____
   |    |      
   |    o      
   |   /|\     
   |    |
   |   / \
  _|_
 |   |______
 |          |
 |__________|
		`
	case 1:
		drw = `
    ____
   |    |      
   |    o      
   |   /|\     
   |    |
   |    
  _|_
 |   |______
 |          |
 |__________|
		`
	case 2:
		drw = `
    ____
   |    |      
   |    o      
   |    |
   |    |
   |     
  _|_
 |   |______
 |          |
 |__________|
		`
	case 3:
		drw = `
    ____
   |    |      
   |    o      
   |        
   |   
   |   
  _|_
 |   |______
 |          |
 |__________|
		`
	case 4:
		drw = `
    ____
   |    |      
   |      
   |      
   |  
   |  
  _|_
 |   |______
 |          |
 |__________|
		`
	case 5:
		drw = `
    ____
   |        
   |        
   |        
   |   
   |   
  _|_
 |   |______
 |          |
 |__________|
		`
	case 6:
		drw = `
    
   |     
   |     
   |     
   |
   |
  _|_
 |   |______
 |          |
 |__________|
		`
	case 7:
		drw = `
  _ _
 |   |______
 |          |
 |__________|
		`
	case 8:
		drw = `

		`
	}
	fmt.Println(drw)
}

func drawState(g *Game, prop string) {
	fmt.Print("Proposed: ")
	drawLetters(g.FoundLetters)

	fmt.Print("Used: ")
	drawLetters(g.UsedLetters)

	switch g.State {
	case "goodProp":
		fmt.Print("Good proposition !")
	case "alreadyProposed":
		fmt.Printf("Letter %v was already proposed!", prop)
	case "badProp":
		fmt.Printf("Damn, %v is not the word!", prop)
	case "lost":
		fmt.Print(`
  ▄████  ▄▄▄       ███▄ ▄███▓▓█████     ▒█████   ██▒   █▓▓█████  ██▀███  
 ██▒ ▀█▒▒████▄    ▓██▒▀█▀ ██▒▓█   ▀    ▒██▒  ██▒▓██░   █▒▓█   ▀ ▓██ ▒ ██▒
▒██░▄▄▄░▒██  ▀█▄  ▓██    ▓██░▒███      ▒██░  ██▒ ▓██  █▒░▒███   ▓██ ░▄█ ▒
░▓█  ██▓░██▄▄▄▄██ ▒██    ▒██ ▒▓█  ▄    ▒██   ██░  ▒██ █░░▒▓█  ▄ ▒██▀▀█▄  
░▒▓███▀▒ ▓█   ▓██▒▒██▒   ░██▒░▒████▒   ░ ████▓▒░   ▒▀█░  ░▒████▒░██▓ ▒██▒
 ░▒   ▒  ▒▒   ▓▒█░░ ▒░   ░  ░░░ ▒░ ░   ░ ▒░▒░▒░    ░ ▐░  ░░ ▒░ ░░ ▒▓ ░▒▓░
  ░   ░   ▒   ▒▒ ░░  ░      ░ ░ ░  ░     ░ ▒ ▒░    ░ ░░   ░ ░  ░  ░▒ ░ ▒░
░ ░   ░   ░   ▒   ░      ░      ░      ░ ░ ░ ▒       ░░     ░     ░░   ░ 
      ░       ░  ░       ░      ░  ░       ░ ░        ░     ░  ░   ░     
                                                     ░                   
`)
		fmt.Print("The word was: ")
		drawLetters(g.Letters)
	case "won":
		fmt.Print(`
  ______   ______    __   __    _______  ______          ___    ___________     _______ 
 /      | /  __  \  |  \ |  |  /  _____||   _  \        /   \  |           |   /       |
|  ,---- |  |  |  | |   \|  | |  |  __  |  |_)  |      /  ^  \  ---|  |----   |   (----
|  |     |  |  |  | |  .    | |  | |_ | |      /      /  /_\  \    |  |        \   \    
|   ----.|   --   | |  |\   | |  |__| | |  |\  \----./  _____  \   |  |    .----)   |   
 \______| \______/  |__| \__|  \______| | _|  ._____/__/     \__\  |__|    |_______/

	`)
		fmt.Print("The word was: ")
		drawLetters(g.Letters)
	}
}

func drawLetters(l []string)  {
	for _, c := range l{
		fmt.Printf("%v ", c)
	}
	fmt.Println()
}