package dictionnary

import (
	"bufio"
	"math/rand"
	"os"
	"time"
)

var words = make([]string, 0, 50)

func Load(filename string) error {
	f, err := os.Open(filename)
	if err != nil { return err }
	defer f.Close()
	s := bufio.NewScanner(f)
	for s.Scan() {
		words = append(words, s.Text())
	}
	if err := s.Err(); err != nil { return err }
	return nil
}

func PickRandWord() string {
	rand.Seed(time.Now().UnixNano())
	i := rand.Intn(len(words))
	return words[i]
}